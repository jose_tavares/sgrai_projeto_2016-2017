#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <windows.h>



#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#ifdef _WIN32
#include <GL/glaux.h>
#endif

#include <AL\alut.h>

#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"
#include "Main.cpp"

using namespace std;

#pragma comment (lib, "glaux.lib")    /* link with Win32 GLAUX lib usada para ler bitmaps */
#pragma comment( user, "Compiled on " __DATE__ " at " __TIME__ ) 

// fun�cao para ler jpegs do ficheiro readjpeg.c
extern "C" int read_JPEG_file(const char *, char **, int *, int *, int *);

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)

// Define medidas das janelas
#define	GAP					         25
#define MAIN_WIN_WIDTH				800
#define MAIN_WIN_HEIGHT				600
#define TOP_SUB_WIN_WIDTH			250
#define TOP_SUB_WIN_HEIGHT			250
#define NAV_SUB_WIN_WIDTH			250
#define NAV_SUB_WIN_HEIGHT			250

// Define medidadas do labirinto, matriz
#define MAZE_HEIGHT			          18
#define MAZE_WIDTH			          18

// Define caracteristicas do boneco
#define	OBJECTO_ALTURA		      0.4
#define OBJECTO_VELOCIDADE	      0.5
#define OBJECTO_ROTACAO		        5
#define OBJECTO_RAIO		      0.12
#define SCALE_AVATAR1              0.005
#define SCALE_AVATAR2              0.005
#define EYE_ROTACAO			        1

// Define caracteristicas dos objectos de pontuacao

// Define carateristicas das texturas
#ifdef _WIN32
#define NOME_TEXTURA_CUBOS        "Textura.bmp"
#define NOME_TEXTURA_CUBOS2       "Lado.bmp"
#else
#define NOME_TEXTURA_CUBOS        "Textura.jpg"
#define NOME_TEXTURA_CUBOS2       "Lado.jpg"
#endif

#define NUM_JOGADORES			  2
#define JOGADOR1				  0
#define JOGADOR2				  1

#define NOME_TEXTURA_CHAO        "Chao.jpg"
#define AVATAR1					"homer.mdl"
#define AVATAR2					"marvin.mdl"

#define NUM_TEXTURAS              3
#define ID_TEXTURA_CHAO           0
#define ID_TEXTURA_CUBOS          1
#define ID_TEXTURA_CUBOS2         2

#define	CHAO_DIMENSAO		      10 //original 10

#define NUM_JANELAS               4
#define JANELA_TOP                0
#define JANELA_NAVIGATE           1
#define JANELA_TOP2               2
#define JANELA_NAVIGATE2          3

#define P_ESFERA				100
#define P_OCTAEDRO				200
#define P_DONUT					300

int pontuacaoP1 = 0;
int pontuacaoP2 = 0;
int num_pontos = 0;
boolean lido = false;

// Estruturas
typedef struct teclas_t {
	GLboolean   up, down, left, right, w, W, s, S, a, A, d, D, e, E, i, I, k, K, j, J, l, L, o, O;
}teclas_t;

typedef struct pos_t {
	GLfloat    x, y, z, x2, y2, z2;
}pos_t;

typedef struct objecto_t {
	pos_t    pos;
	GLfloat  dir;
	GLfloat  vel;
}objecto_t;

typedef struct camera_t {
	pos_t    eye;
	GLfloat  dir_long;  // longitude olhar (esq-dir)
	GLfloat  dir_lat;   // latitude olhar	(cima-baixo)
	GLfloat  fov;
}camera_t;

typedef struct ESTADO {
	camera_t      camera, camera2;
	GLint         timer;
	GLint         mainWindow, topSubwindowP1, navigateSubwindowP1, topSubwindowP2, navigateSubwindowP2;
	teclas_t      teclas;
	GLboolean     localViewer;
	GLuint        vista[NUM_JANELAS];
	GLuint        nevoeiro[NUM_JOGADORES];
	ALuint		  buffer[15], source[7];
}ESTADO;

typedef struct MODELO {
	GLuint        texID[NUM_JANELAS][NUM_TEXTURAS];
	GLuint        labirinto[NUM_JANELAS];
	GLuint        chao[NUM_JANELAS];
	objecto_t	  objecto, objecto2;
	GLuint        xMouse;
	GLuint        yMouse;
	StudioModel   playerP1[NUM_JANELAS], playerP2[NUM_JANELAS];
	GLboolean     andar, andar2;
	GLuint        prev, prev2;
	GLfloat		  rotacao;
}MODELO;

/////////////////////////////////////
//variaveis globais

ESTADO estado;
MODELO modelo;

char mazedata[MAZE_HEIGHT][MAZE_WIDTH + 1];

void lerMapa(string mapa) {
	string linha;
	int inic = 0, i = 0, j = 0;
	ifstream file;

	file.open("mapas\\" + mapa + ".txt");
	while (!file.eof()) {
		getline(file, linha, '\n');
	}

	if (linha.size() > 0) {
		string strCubo[MAZE_HEIGHT];
		int pos;

		for (i = 0; i < MAZE_HEIGHT; i++) {
			pos = linha.find(',', inic);
			strCubo[i] = linha.substr(inic, pos - inic);
			for (j = 0; j < strCubo[i].size(); j++) {
				mazedata[i][j] = strCubo[i].at(j);
				if (mazedata[i][j] == 'd' || mazedata[i][j] == 's' || mazedata[i][j] == 'o') {
					num_pontos++;

				}
			}
			pos++;
			inic = pos;

		}
		lido = true;
	}
}

////////////////////////////////////
/// Ilumina��o e materiais

void setLight()
{
	GLfloat light_pos[4] = { -5.0, 20.0, -8.0, 0.0 };
	GLfloat light_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat light_diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat light_specular[] = { 0.5f, 0.5f, 0.5f, 1.0f };

	// ligar ilumina��o
	glEnable(GL_LIGHTING);

	// ligar e definir fonte de luz 0
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, estado.localViewer);
}

void setMaterial()
{
	GLfloat mat_specular[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mat_shininess = 104;

	// cria��o autom�tica das componentes Ambiente e Difusa do material a partir das cores
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	// definir de outros par�metros dos materiais est�ticamente
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}

///////////////////////////////////
//// Redisplays

void redisplayTopSubwindowP1(int width, int height)
{
	// glViewport(botom, left, width, height)// define parte da janela a ser utilizada pelo OpenGL
	glViewport(0, 0, (GLint)width, (GLint)height);
	// Matriz Projeccao	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (GLfloat)width / height, .5, 100);
	// Matriz Modelview	// Matriz onde s�o realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
}

void reshapeNavigateSubwindowP1(int width, int height)
{
	glViewport(0, 0, (GLint)width, (GLint)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(estado.camera.fov, (GLfloat)width / height, 0.1, 50);
	glMatrixMode(GL_MODELVIEW);
}

void redisplayTopSubwindowP2(int width, int height)
{
	glViewport(0, 0, (GLint)width, (GLint)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (GLfloat)width / height, .5, 100);
	glMatrixMode(GL_MODELVIEW);
}

void reshapeNavigateSubwindowP2(int width, int height)
{
	glViewport(0, 0, (GLint)width, (GLint)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(estado.camera2.fov, (GLfloat)width / height, 0.1, 50);
	glMatrixMode(GL_MODELVIEW);
}

void reshapeMainWindow(int width, int height)
{
	GLint w, h;
	w = (width - GAP * 3)*.5;
	h = (height - GAP * 2);
	glutSetWindow(estado.topSubwindowP1);
	glutPositionWindow(GAP, GAP);
	glutReshapeWindow(w, h / 2);
	glutSetWindow(estado.navigateSubwindowP1);
	glutPositionWindow(GAP + w + GAP, GAP);
	glutReshapeWindow(w, h / 2);
	glutSetWindow(estado.topSubwindowP2);
	glutPositionWindow(GAP, GAP + h / 2 + GAP);
	glutReshapeWindow(w, h / 2);
	glutSetWindow(estado.navigateSubwindowP2);
	glutPositionWindow(GAP + w + GAP, GAP + h / 2 + GAP);
	glutReshapeWindow(w, h / 2);

}

///Letras N e S
void strokeCenterString(char *str, double x, double y, double z, double s)
{
	int i, n;

	n = strlen(str);
	glPushMatrix();
	glTranslated(x - glutStrokeLength(GLUT_STROKE_ROMAN, (const unsigned char*)str)*0.5*s, y - 119.05*0.5*s, z);
	glScaled(s, s, s);
	for (i = 0; i < n; i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, (int)str[i]);
	glPopMatrix();
}

//Modelo
GLboolean detectaColisaoP1(GLfloat nx, GLfloat nz)
{
	GLuint i = (nx + MAZE_HEIGHT*0.5 + 0.5), j = (int)(nz + MAZE_WIDTH*0.5 + 0.5);
	if (mazedata[i][j] == '*')
	{
		if (modelo.playerP1[JANELA_NAVIGATE].GetSequence() != 20 && modelo.playerP1[JANELA_NAVIGATE2].GetSequence() != 20)
		{
			modelo.playerP1[JANELA_TOP].SetSequence(20);
			modelo.playerP1[JANELA_NAVIGATE].SetSequence(20);

			modelo.playerP1[JANELA_TOP2].SetSequence(20);
			modelo.playerP1[JANELA_NAVIGATE2].SetSequence(20);
		}
		alSourceStop(estado.source[2]);
		alSourcei(estado.source[2], AL_BUFFER, estado.buffer[(rand() % (3 + 1 - 2)) + 2]); //(rand() % (max + 1 - min)) + min
		alSourcePlay(estado.source[2]);
		return(GL_TRUE);
	}
	else if (mazedata[i][j] == 'd' || mazedata[i][j] == 'o' || mazedata[i][j] == 's') {
		alSourceStop(estado.source[5]);
		alSourcei(estado.source[5], AL_BUFFER, estado.buffer[(rand() % (11 + 1 - 8)) + 8]); //(rand() % (max + 1 - min)) + min
		alSourcePlay(estado.source[5]);
	}
	return GL_FALSE;
}

GLboolean detectaColisaoP2(GLfloat nx, GLfloat nz)
{
	GLuint i = (nx + MAZE_HEIGHT*0.5 + 0.5), j = (int)(nz + MAZE_WIDTH*0.5 + 0.5);
	if (mazedata[i][j] == '*')
	{
		if (modelo.playerP2[JANELA_NAVIGATE].GetSequence() != 20 && modelo.playerP2[JANELA_NAVIGATE2].GetSequence() != 20)
		{
			modelo.playerP2[JANELA_TOP].SetSequence(20);
			modelo.playerP2[JANELA_NAVIGATE].SetSequence(20);

			modelo.playerP2[JANELA_TOP2].SetSequence(20);
			modelo.playerP2[JANELA_NAVIGATE2].SetSequence(20);
		}
		alSourceStop(estado.source[3]);
		alSourcei(estado.source[3], AL_BUFFER, estado.buffer[(rand() % (5 + 1 - 4)) + 4]); //(rand() % (max + 1 - min)) + min
		alSourcePlay(estado.source[3]);
		return(GL_TRUE);
	}
	else if (mazedata[i][j] == 'd' || mazedata[i][j] == 'o' || mazedata[i][j] == 's') {
		alSourceStop(estado.source[6]);
		alSourcei(estado.source[6], AL_BUFFER, estado.buffer[(rand() % (14 + 1 - 12)) + 12]); //(rand() % (max + 1 - min)) + min
		alSourcePlay(estado.source[6]);
	}
	return GL_FALSE;
}

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[], GLfloat tx, GLfloat ty)
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	glTexCoord2f(tx + 0, ty + 0);
	glVertex3fv(a);
	glTexCoord2f(tx + 0, ty + 0.25);
	glVertex3fv(b);
	glTexCoord2f(tx + 0.25, ty + 0.25);
	glVertex3fv(c);
	glTexCoord2f(tx + 0.25, ty + 0);
	glVertex3fv(d);
	glEnd();
}

void desenhaCubo(int tipo, GLuint texID)
{
	GLfloat vertices[][3] = { {-0.5,-0.5,-0.5},{0.5,-0.5,-0.5},{0.5,0.5,-0.5},{-0.5,0.5,-0.5},{-0.5,-0.5,0.5},
							  {0.5,-0.5,0.5},{0.5,0.5,0.5},{-0.5,0.5,0.5} };
	GLfloat normais[][3] = { {0,0,-1},{ 0,1,0 },{ -1,0,0 },{ 1,0,0 },{ 0,0,1 },{ 0,-1,0 } };

	GLfloat tx, ty;

	switch (tipo)
	{
	case 0: tx = 0, ty = 0;
		break;
	case 1: tx = 0, ty = 0.25;
		break;
	case 2: tx = 0, ty = 0.5;
		break;
	case 3: tx = 0, ty = 0.75;
		break;
	case 4: tx = 0.25, ty = 0;
		break;

	default:
		tx = 0.75, ty = 0.75;
	}
	glBindTexture(GL_TEXTURE_2D, texID + 1);



	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[5], tx, ty);

	glBindTexture(GL_TEXTURE_2D, texID);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], tx, ty);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[3], tx, ty);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[0], tx, ty);

	glBindTexture(GL_TEXTURE_2D, texID + 1);

	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[2], tx, ty);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[4], tx, ty);

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaBussola(int width, int height)  // largura e altura da janela
{

	// Altera viewport e projec��o para 2D (copia de um reshape de um projecto 2D)
	glViewport(width - 60, 0, 60, 60);
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	gluOrtho2D(-30, 30, -30, 30);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_COLOR_MATERIAL);
	glRotatef(GRAUS(estado.camera.dir_long + estado.camera.dir_lat) - 90, 0, 0, 1);

	glScaled(0.70, 0.70, 0.70);

	glBegin(GL_TRIANGLES);
	glColor4f(0, 0, 0, 0.2);
	glVertex2f(0, 15);
	glVertex2f(-6, 0);
	glVertex2f(6, 0);
	glColor4f(1, 1, 1, 0.2);
	glVertex2f(6, 0);
	glVertex2f(-6, 0);
	glVertex2f(0, -15);
	glEnd();

	glLineWidth(1.0);
	glColor3f(1, 0.4, 0.4);
	strokeCenterString("N", 0, 20, 0, 0.1);
	strokeCenterString("S", 0, -20, 0, 0.1);
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);

	//....

	// Blending (transparencias)
  /*  glEnable(GL_BLEND);
	  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	  glDisable(GL_LIGHTING);
	  glDisable(GL_DEPTH_TEST);
	  glDisable(GL_COLOR_MATERIAL);
  */

  //desenha bussola 2D

	//glColor3f(1,0.4,0.4);
	//strokeCenterString("N", 0, 20, 0 , 0.1); // string, x ,y ,z ,scale


  // rep�e estado
 /* glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);
*/

//rep�e projec��o chamando redisplay
  //reshapeNavigateSubwindow(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	reshapeNavigateSubwindowP1(width, height);
}

void desenhaBussola2(int width, int height)  // largura e altura da janela
{

	// Altera viewport e projec��o para 2D (copia de um reshape de um projecto 2D)
	glViewport(width - 60, 0, 60, 60);
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	gluOrtho2D(-30, 30, -30, 30);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_COLOR_MATERIAL);
	glRotatef(GRAUS(estado.camera2.dir_long + estado.camera2.dir_lat) - 90, 0, 0, 1);

	glScaled(0.70, 0.70, 0.70);

	glBegin(GL_TRIANGLES);
	glColor4f(0, 0, 0, 0.2);
	glVertex2f(0, 15);
	glVertex2f(-6, 0);
	glVertex2f(6, 0);
	glColor4f(1, 1, 1, 0.2);
	glVertex2f(6, 0);
	glVertex2f(-6, 0);
	glVertex2f(0, -15);
	glEnd();

	glLineWidth(1.0);
	glColor3f(1, 0.4, 0.4);
	strokeCenterString("N", 0, 20, 0, 0.1);
	strokeCenterString("S", 0, -20, 0, 0.1);
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);

	reshapeNavigateSubwindowP2(width, height);
}

void desenhaPontuacao1(int width, int height)  // largura e altura da janela
{

	// Altera viewport e projec��o para 2D (copia de um reshape de um projecto 2D)
	glViewport(width - 60, 0, 60, 60);
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	gluOrtho2D(-50, 55, -30, 55);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_COLOR_MATERIAL);
	//glRotatef(0, 0, 0, 0);


	//glTranslatef(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);

	glColor3f(0, 0, 0);
	int i;
	int x;
	char aux2[20];


	sprintf(aux2, "Pontua��o : %d", pontuacaoP1);

	x = strlen(aux2);

	glTranslated(-48, 45, 0);
	glScaled(0.1, 0.1, 0.1);

	for (i = 0; i < x; i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, (int)aux2[i]);



	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);

	reshapeNavigateSubwindowP1(width, height);
}

void desenhaPontuacao2(int width, int height)  // largura e altura da janela
{
	glViewport(width - 60, 0, 60, 60);
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	gluOrtho2D(-50, 55, -30, 55);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_COLOR_MATERIAL);
	//glRotatef(0, 0, 0, 0);


	glColor3f(0, 0, 0);
	int i;
	int x;
	char aux[30];

	sprintf(aux, "Pontua��o : %d", pontuacaoP2);

	x = strlen(aux);

	glTranslated(-48, 45, 0);

	glScaled(0.1, 0.1, 0.1);

	for (i = 0; i < x; i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, (int)aux[i]);



	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);


	reshapeNavigateSubwindowP2(width, height);
}

void desenhaModeloDir(objecto_t obj, int width, int height)
{
	// Altera viewport e projec��o
	glViewport(width - 60, 0, 60, 60);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-10, 10, -10, 10);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glRotatef(GRAUS(obj.dir), 0, 0, 1);

	glBegin(GL_QUADS);
	glColor4f(1, 0, 0, 0.5);
	glVertex2f(5, 2.5);
	glVertex2f(-10, 2.5);
	glVertex2f(-10, -2.5);
	glVertex2f(5, -2.5);
	glEnd();
	glBegin(GL_TRIANGLES);
	glVertex2f(10, 0);
	glVertex2f(5, 5);
	glVertex2f(5, -5);
	glEnd();

	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	// Blending (transparencias)
  /*
	glEnable(GL_BLEND);
	  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	  glDisable(GL_LIGHTING);
	  glDisable(GL_DEPTH_TEST);
  */

  //desenha Seta

  // rop�e estado
/*  glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
*/
//rep�e projec��o chamando redisplay
	redisplayTopSubwindowP1(width, height);
}

void desenhaAngVisao(camera_t *cam)
{
	GLfloat ratio;
	ratio = (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / glutGet(GLUT_WINDOW_HEIGHT); // propor��o 
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	glPushMatrix();
	glTranslatef(cam->eye.x, OBJECTO_ALTURA, cam->eye.z);
	glColor4f(0, 0, 1, 0.2);
	glRotatef(GRAUS(cam->dir_long), 0, 1, 0);

	glBegin(GL_TRIANGLES);
	glVertex3f(0, 0, 0);
	glVertex3f(5 * cos(RAD(cam->fov*ratio*0.5)), 0, -5 * sin(RAD(cam->fov*ratio*0.5)));
	glVertex3f(5 * cos(RAD(cam->fov*ratio*0.5)), 0, 5 * sin(RAD(cam->fov*ratio*0.5)));
	glEnd();
	glPopMatrix();

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}

void desenhaAngVisao2(camera_t *cam)
{
	GLfloat ratio;
	ratio = (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / glutGet(GLUT_WINDOW_HEIGHT); // propor��o 
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	glPushMatrix();
	glTranslatef(cam->eye.x2, OBJECTO_ALTURA, cam->eye.z2);
	glColor4f(0, 0, 1, 0.2);
	glRotatef(GRAUS(cam->dir_long), 0, 1, 0);

	glBegin(GL_TRIANGLES);
	glVertex3f(0, 0, 0);
	glVertex3f(5 * cos(RAD(cam->fov*ratio*0.5)), 0, -5 * sin(RAD(cam->fov*ratio*0.5)));
	glVertex3f(5 * cos(RAD(cam->fov*ratio*0.5)), 0, 5 * sin(RAD(cam->fov*ratio*0.5)));
	glEnd();
	glPopMatrix();

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}

void desenhaModelo()
{
	glColor3f(0, 1, 0);
	glutSolidCube(OBJECTO_ALTURA);
	glPushMatrix();
	glColor3f(1, 0, 0);
	glTranslatef(0, OBJECTO_ALTURA*0.75, 0);
	glRotatef(GRAUS(estado.camera.dir_long - modelo.objecto.dir), 0, 1, 0);
	glutSolidCube(OBJECTO_ALTURA*0.5);
	glPopMatrix();
}

void desenhaLabirinto(GLuint texID)
{
	int i, j;
	glColor3f(0.8f, 0.8f, 0.8f);
	glPushMatrix();
	glTranslatef(-MAZE_HEIGHT*0.5, 0.5, -MAZE_WIDTH*0.5);
	for (i = 0; i < MAZE_HEIGHT; i++)
		for (j = 0; j < MAZE_WIDTH; j++)
			if (mazedata[i][j] == '*')
			{
				glPushMatrix();
				glTranslatef(i, 0, j);
				desenhaCubo((i + j) % 6, texID);
				glPopMatrix();
			}
	glPopMatrix();
}

void desenhaDognut(GLfloat rotacao) {
	int i, j;
	glPushMatrix();
	//glColor3ub(134, 244, 66);		

	//Gold
	GLfloat ambient[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
	GLfloat diffuse[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
	GLfloat specular[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
	GLfloat shine = 0.4f;

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);

	glTranslatef(-MAZE_HEIGHT*0.5, 0.5, -MAZE_WIDTH*0.5);
	for (i = 0; i < MAZE_HEIGHT; i++)
		for (j = 0; j < MAZE_WIDTH; j++)
			if (mazedata[i][j] == 's')
			{

				glPushMatrix();
				glTranslatef(i, -0.3, j);
				glRotatef(rotacao, 1.0, 1.0, 1.0);
				glutSolidTorus(0.05, 0.08, 10, 50);
				glPopMatrix();
			}
	glPopMatrix();
}

void desenhaOctaedro(GLfloat rotacao) {
	int i, j;
	//glColor3ub(244, 122, 66);
	glPushMatrix();

	//Ruby
	GLfloat ambient[] = { 0.1745f, 0.01175f, 0.01175f, 0.55f };
	GLfloat diffuse[] = { 0.61424f, 0.04136f, 0.04136f, 0.55f };
	GLfloat specular[] = { 0.727811f, 0.626959f, 0.626959f, 0.55f };
	GLfloat shine = 0.6f;

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);

	glTranslatef(-MAZE_HEIGHT*0.5, 0.5, -MAZE_WIDTH*0.5);
	for (i = 0; i < MAZE_HEIGHT; i++)
		for (j = 0; j < MAZE_WIDTH; j++)
			if (mazedata[i][j] == 'o')
			{

				glPushMatrix();
				glTranslatef(i, -0.3, j);
				glScalef(0.1, 0.1, 0.1);
				glRotatef(rotacao, 1.0, 1.0, 1.0);
				glutSolidOctahedron();
				glPopMatrix();
			}
	glPopMatrix();
}

void desenhaDodecahedro(GLfloat rotacao) {
	int i, j;
	//glColor3ub(244, 122, 66);
	glPushMatrix();

	//Esmerald
	GLfloat ambient[] = { 0.0215f, 0.1745f, 0.0215f, 0.55f };
	GLfloat diffuse[] = { 0.07568f, 0.61424f, 0.07568f, 0.55f };
	GLfloat specular[] = { 0.633f, 0.727811f, 0.633f, 0.55f };
	GLfloat shine = 0.6f;

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);

	glTranslatef(-MAZE_HEIGHT*0.5, 0.5, -MAZE_WIDTH*0.5);
	for (i = 0; i < MAZE_HEIGHT; i++)
		for (j = 0; j < MAZE_WIDTH; j++)
			if (mazedata[i][j] == 'd')
			{

				glPushMatrix();
				glTranslatef(i, -0.3, j);
				glScalef(0.05, 0.05, 0.05);
				glRotatef(rotacao, 1.0, 1.0, 1.0);
				glutSolidDodecahedron();
				glPopMatrix();
			}
	glPopMatrix();
}

#define STEP    1

void desenhaChao(GLfloat dimensao, GLuint texID)
{
	// c�digo para desenhar o ch�o
	GLfloat i, j;
	glBindTexture(GL_TEXTURE_2D, texID);

	glColor3f(0.5f, 0.5f, 0.5f);
	for (i = -dimensao; i <= dimensao; i += STEP)
		for (j = -dimensao; j <= dimensao; j += STEP)
		{
			glBegin(GL_POLYGON);
			glNormal3f(0, 1, 0);
			glTexCoord2f(1, 1);
			glVertex3f(i + STEP, 0, j + STEP);
			glTexCoord2f(0, 1);
			glVertex3f(i, 0, j + STEP);
			glTexCoord2f(0, 0);
			glVertex3f(i, 0, j);
			glTexCoord2f(1, 0);
			glVertex3f(i + STEP, 0, j);
			glEnd();
		}
	glBindTexture(GL_TEXTURE_2D, NULL);
}

// Navegacao com o rato Jogador1
void motionNavigateSubwindowP1(int x, int y)
{
	int dif;
	dif = y - modelo.yMouse;

	if (dif > 0) {//olhar para baixo
		estado.camera.dir_lat -= dif*RAD(EYE_ROTACAO);
		if (estado.camera.dir_lat < -RAD(45))
			estado.camera.dir_lat = -RAD(45);
	}

	if (dif < 0) {//olhar para cima
		estado.camera.dir_lat += abs(dif)*RAD(EYE_ROTACAO);
		if (estado.camera.dir_lat > RAD(45))
			estado.camera.dir_lat = RAD(45);
	}

	dif = x - modelo.xMouse;

	if (dif > 0) { //olhar para a direita
		estado.camera.dir_long -= dif*RAD(EYE_ROTACAO);
		/*
		if(estado.camera.dir_long<modelo.objecto.dir-RAD(45))
		estado.camera.dir_long=modelo.objecto.dir-RAD(45);
		*/
	}

	if (dif < 0) {//olhar para a esquerda
		estado.camera.dir_long += abs(dif)*RAD(EYE_ROTACAO);
		/*
		if(estado.camera.dir_long>modelo.objecto.dir+RAD(45))
		estado.camera.dir_long=modelo.objecto.dir+RAD(45);
		*/
	}

	modelo.xMouse = x;
	modelo.yMouse = y;
}

void mouseNavigateSubwindowP1(int button, int state, int x, int y)
{
	if (button == GLUT_RIGHT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			modelo.xMouse = x;
			modelo.yMouse = y;
			glutMotionFunc(motionNavigateSubwindowP1);
		}
		else
			glutMotionFunc(NULL);
	}
}

// Navegacao com o rato Jogador2
void motionNavigateSubwindowP2(int x, int y)
{
	int dif;
	dif = y - modelo.yMouse;

	if (dif > 0) {//olhar para baixo
		estado.camera2.dir_lat -= dif*RAD(EYE_ROTACAO);
		if (estado.camera2.dir_lat < -RAD(45))
			estado.camera2.dir_lat = -RAD(45);
	}

	if (dif < 0) {//olhar para cima
		estado.camera2.dir_lat += abs(dif)*RAD(EYE_ROTACAO);
		if (estado.camera2.dir_lat > RAD(45))
			estado.camera2.dir_lat = RAD(45);
	}

	dif = x - modelo.xMouse;

	if (dif > 0) { //olhar para a direita
		estado.camera2.dir_long -= dif*RAD(EYE_ROTACAO);
		/*
		if(estado.camera.dir_long<modelo.objecto.dir-RAD(45))
		estado.camera.dir_long=modelo.objecto.dir-RAD(45);
		*/
	}

	if (dif < 0) {//olhar para a esquerda
		estado.camera2.dir_long += abs(dif)*RAD(EYE_ROTACAO);
		/*
		if(estado.camera.dir_long>modelo.objecto.dir+RAD(45))
		estado.camera.dir_long=modelo.objecto.dir+RAD(45);
		*/
	}

	modelo.xMouse = x;
	modelo.yMouse = y;
}

void mouseNavigateSubwindowP2(int button, int state, int x, int y)
{
	if (button == GLUT_RIGHT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			modelo.xMouse = x;
			modelo.yMouse = y;
			glutMotionFunc(motionNavigateSubwindowP2);
		}
		else
			glutMotionFunc(NULL);
	}
}

// Camara de seguimento jogador1
void setNavigateSubwindowP1Camera(camera_t *cam, objecto_t obj)
{
	pos_t center;

	if (estado.vista[JANELA_NAVIGATE])
	{
		cam->eye.x = obj.pos.x;
		cam->eye.y = obj.pos.y + .2;
		cam->eye.z = obj.pos.z;
		center.x = obj.pos.x + cos(cam->dir_long)*cos(cam->dir_lat);
		center.z = obj.pos.z + sin(-cam->dir_long)*cos(cam->dir_lat);
		center.y = cam->eye.y + sin(cam->dir_lat);
	}
	else
	{
		center.x = obj.pos.x;
		center.y = obj.pos.y + 0.5;
		center.z = obj.pos.z;

		cam->eye.x = center.x - cos(cam->dir_long);
		cam->eye.z = center.z - sin(-cam->dir_long);
		cam->eye.y = center.y + 0.5;
	}
	gluLookAt(cam->eye.x, cam->eye.y, cam->eye.z, center.x, center.y, center.z, 0, 1, 0);

	//FOG RED BOOK
	{
		GLfloat fogColor[4] = { 0.5, 0.5, 0.5, 1.0 };

		static GLint fogMode = GL_EXP;
		glFogi(GL_FOG_MODE, fogMode);
		glFogfv(GL_FOG_COLOR, fogColor);
		glFogf(GL_FOG_DENSITY, 0.35);
		glHint(GL_FOG_HINT, GL_DONT_CARE);
		glFogf(GL_FOG_START, 1.0);
		glFogf(GL_FOG_END, 5.0);
	}
	glClearColor(0.7, 0.7, 0.7, 1.0);  /* fog color */

	if (estado.nevoeiro[JOGADOR1]) {
		glEnable(GL_FOG);
	}
	else {
		glDisable(GL_FOG);
	}
}

// Janela de navegacao 1a pessoa jogador1
void displayNavigateSubwindowP1()
{
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
	glClearDepth(1);
	glEnable(GL_DEPTH_TEST);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

	setNavigateSubwindowP1Camera(&estado.camera, modelo.objecto);
	setLight();

	glPushMatrix();
	DrawGLScene();
	glPopMatrix();

	glCallList(modelo.labirinto[JANELA_NAVIGATE]);
	glCallList(modelo.chao[JANELA_NAVIGATE]);


	if (!estado.vista[JANELA_NAVIGATE])
	{
		//desenha boneco Jogador1
		glPushMatrix();
		glTranslatef(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
		glRotatef(GRAUS(modelo.objecto.dir), 0, 1, 0);
		glRotatef(-90, 1, 0, 0);
		glScalef(SCALE_AVATAR1, SCALE_AVATAR1, SCALE_AVATAR1);
		mdlviewer_display(modelo.playerP1[JANELA_NAVIGATE]);
		glPopMatrix();
		//desenha boneco Jogador2
		glPushMatrix();
		glTranslatef(modelo.objecto2.pos.x2, modelo.objecto2.pos.y2, modelo.objecto2.pos.z2);
		glRotatef(GRAUS(modelo.objecto2.dir), 0, 1, 0);
		glRotatef(-90, 1, 0, 0);
		glScalef(SCALE_AVATAR2, SCALE_AVATAR2, SCALE_AVATAR2);
		mdlviewer_display(modelo.playerP2[JANELA_NAVIGATE]);
		glPopMatrix();
		// desenha solidos da pontuacao
		desenhaDognut(modelo.rotacao);
		desenhaOctaedro(modelo.rotacao);
		desenhaDodecahedro(modelo.rotacao);
	}

	desenhaPontuacao1(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	desenhaBussola(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));


	glutSwapBuffers();
}

//Janela vista de cima jogador1
void setTopSubwindowP1Camera(camera_t *cam, objecto_t obj)
{
	cam->eye.x = obj.pos.x;
	cam->eye.z = obj.pos.z;
	if (estado.vista[JANELA_TOP])
		gluLookAt(obj.pos.x, CHAO_DIMENSAO*.2, obj.pos.z, obj.pos.x, obj.pos.y, obj.pos.z, 0, 0, -1);
	else
		gluLookAt(obj.pos.x, CHAO_DIMENSAO * 2, obj.pos.z, obj.pos.x, obj.pos.y, obj.pos.z, 0, 0, -1);
}

void displayTopSubwindowP1()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	setTopSubwindowP1Camera(&estado.camera, modelo.objecto);
	setLight();

	glCallList(modelo.labirinto[JANELA_TOP]);
	glCallList(modelo.chao[JANELA_TOP]);

	glPushMatrix();
	glTranslatef(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
	glRotatef(GRAUS(modelo.objecto.dir), 0, 1, 0);
	glRotatef(-90, 1, 0, 0);
	glScalef(SCALE_AVATAR1, SCALE_AVATAR1, SCALE_AVATAR1);
	mdlviewer_display(modelo.playerP1[JANELA_TOP]);
	glPopMatrix();
	// desenha solidos da pontuacao
	desenhaDognut(modelo.rotacao);
	desenhaOctaedro(modelo.rotacao);
	desenhaDodecahedro(modelo.rotacao);

	glPushMatrix();
	DrawGLScene();
	glPopMatrix();

	desenhaAngVisao(&estado.camera);
	desenhaModeloDir(modelo.objecto, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	glutSwapBuffers();
}

// Camara de seguimento jogador2
void setNavigateSubwindowP2Camera(camera_t *cam, objecto_t obj)
{
	pos_t center;

	if (estado.vista[JANELA_NAVIGATE2])
	{
		cam->eye.x2 = obj.pos.x2;
		cam->eye.y2 = obj.pos.y2 + .2;
		cam->eye.z2 = obj.pos.z2;
		center.x2 = obj.pos.x2 + cos(cam->dir_long)*cos(cam->dir_lat);
		center.z2 = obj.pos.z2 + sin(-cam->dir_long)*cos(cam->dir_lat);
		center.y2 = cam->eye.y2 + sin(cam->dir_lat);
	}
	else
	{
		center.x2 = obj.pos.x2;
		center.y2 = obj.pos.y2 + 0.5;
		center.z2 = obj.pos.z2;

		cam->eye.x2 = center.x2 - cos(cam->dir_long);
		cam->eye.z2 = center.z2 - sin(-cam->dir_long);
		cam->eye.y2 = center.y2 + 0.5;
	}

	gluLookAt(cam->eye.x2, cam->eye.y2, cam->eye.z2, center.x2, center.y2, center.z2, 0, 1, 0);

	//FOG RED BOOK
	{
		GLfloat fogColor[4] = { 0.5, 0.5, 0.5, 1.0 };

		static GLint fogMode = GL_EXP;
		glFogi(GL_FOG_MODE, fogMode);
		glFogfv(GL_FOG_COLOR, fogColor);
		glFogf(GL_FOG_DENSITY, 0.35);
		glHint(GL_FOG_HINT, GL_DONT_CARE);
		glFogf(GL_FOG_START, 1.0);
		glFogf(GL_FOG_END, 5.0);
	}
	glClearColor(0.7, 0.7, 0.7, 1.0);  /* fog color */

	if (estado.nevoeiro[JOGADOR2]) {
		glEnable(GL_FOG);
	}
	else {
		glDisable(GL_FOG);
	}
}

// Janela de navegacao 1a pessoa jogador2
void displayNavigateSubwindowP2()
{
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
	glClearDepth(1);
	glEnable(GL_DEPTH_TEST);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

	setNavigateSubwindowP2Camera(&estado.camera2, modelo.objecto2);
	setLight();

	glPushMatrix();
	DrawGLScene();
	glPopMatrix();

	glCallList(modelo.labirinto[JANELA_NAVIGATE2]);
	glCallList(modelo.chao[JANELA_NAVIGATE2]);

	if (!estado.vista[JANELA_NAVIGATE2])
	{
		//desenha boneco Jogador1
		glPushMatrix();
		glTranslatef(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
		glRotatef(GRAUS(modelo.objecto.dir), 0, 1, 0);
		glRotatef(-90, 1, 0, 0);
		glScalef(SCALE_AVATAR1, SCALE_AVATAR1, SCALE_AVATAR1);
		mdlviewer_display(modelo.playerP1[JANELA_NAVIGATE2]);
		glPopMatrix();
		//desenha boneco Jogador2
		glPushMatrix();
		glTranslatef(modelo.objecto2.pos.x2, modelo.objecto2.pos.y2, modelo.objecto2.pos.z2);
		glRotatef(GRAUS(modelo.objecto2.dir), 0, 1, 0);
		glRotatef(-90, 1, 0, 0);
		glScalef(SCALE_AVATAR2, SCALE_AVATAR2, SCALE_AVATAR2);
		mdlviewer_display(modelo.playerP2[JANELA_NAVIGATE2]);
		glPopMatrix();
		// desenha solidos da pontuacao
		desenhaDognut(modelo.rotacao);
		desenhaOctaedro(modelo.rotacao);
		desenhaDodecahedro(modelo.rotacao);
	}
	desenhaPontuacao2(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	desenhaBussola2(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	glutSwapBuffers();
}

//Janela vista de cima jogador2
void setTopSubwindowP2Camera(camera_t *cam, objecto_t obj)
{
	cam->eye.x2 = obj.pos.x2;
	cam->eye.z2 = obj.pos.z2;
	if (estado.vista[JANELA_TOP2])
		gluLookAt(obj.pos.x2, CHAO_DIMENSAO*.2, obj.pos.z2, obj.pos.x2, obj.pos.y2, obj.pos.z2, 0, 0, -1);
	else
		gluLookAt(obj.pos.x2, CHAO_DIMENSAO * 2, obj.pos.z2, obj.pos.x2, obj.pos.y2, obj.pos.z2, 0, 0, -1);
}

void displayTopSubwindowP2()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	setTopSubwindowP2Camera(&estado.camera2, modelo.objecto2);
	setLight();

	glCallList(modelo.labirinto[JANELA_TOP2]);
	glCallList(modelo.chao[JANELA_TOP2]);

	glPushMatrix();
	glTranslatef(modelo.objecto2.pos.x2, modelo.objecto2.pos.y2, modelo.objecto2.pos.z2);
	glRotatef(GRAUS(modelo.objecto2.dir), 0, 1, 0);
	glRotatef(-90, 1, 0, 0);
	glScalef(SCALE_AVATAR2, SCALE_AVATAR2, SCALE_AVATAR2);
	mdlviewer_display(modelo.playerP2[JANELA_TOP2]);
	glPopMatrix();
	// desenha solidos da pontuacao
	desenhaDognut(modelo.rotacao);
	desenhaOctaedro(modelo.rotacao);
	desenhaDodecahedro(modelo.rotacao);

	glPushMatrix();
	DrawGLScene();
	glPopMatrix();

	desenhaAngVisao2(&estado.camera2);
	desenhaModeloDir(modelo.objecto2, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	glutSwapBuffers();
}

GLboolean detectaPontoP1(GLfloat nx, GLfloat nz) {
	GLuint i = (nx + MAZE_HEIGHT*0.5 + 0.5), j = (int)(nz + MAZE_WIDTH*0.5 + 0.5);
	if (mazedata[i][j] == 's') {
		mazedata[i][j] = ' ';
		num_pontos--;

		pontuacaoP1 = pontuacaoP1 + P_ESFERA;
		glutDisplayFunc(displayNavigateSubwindowP1);
		glutDisplayFunc(displayNavigateSubwindowP2);


		return(GL_TRUE);
	}
	else {
		if (mazedata[i][j] == 'o') {
			mazedata[i][j] = ' ';
			num_pontos--;

			pontuacaoP1 = pontuacaoP1 + P_OCTAEDRO;
			glutDisplayFunc(displayNavigateSubwindowP1);
			glutDisplayFunc(displayNavigateSubwindowP2);


			return(GL_TRUE);
		}
		else {
			if (
				mazedata[i][j] == 'd') {
				mazedata[i][j] = ' ';
				num_pontos--;

				pontuacaoP1 = pontuacaoP1 + P_DONUT;
				glutDisplayFunc(displayNavigateSubwindowP1);
				glutDisplayFunc(displayNavigateSubwindowP2);


				return(GL_TRUE);
			}
		}
		return GL_FALSE;
	}
}
GLboolean detectaPontoP2(GLfloat nx, GLfloat nz) {
	GLuint i = (nx + MAZE_HEIGHT*0.5 + 0.5), j = (int)(nz + MAZE_WIDTH*0.5 + 0.5);
	if (mazedata[i][j] == 's') {
		mazedata[i][j] = ' ';
		num_pontos--;

		pontuacaoP2 = pontuacaoP2 + P_ESFERA;
		glutDisplayFunc(displayNavigateSubwindowP1);
		glutDisplayFunc(displayNavigateSubwindowP2);


		return(GL_TRUE);
	}
	else {
		if (mazedata[i][j] == 'o') {
			mazedata[i][j] = ' ';
			num_pontos--;

			pontuacaoP2 = pontuacaoP2 + P_OCTAEDRO;
			glutDisplayFunc(displayNavigateSubwindowP1);
			glutDisplayFunc(displayNavigateSubwindowP2);


			return(GL_TRUE);
		}
		else {
			if (
				mazedata[i][j] == 'd') {
				mazedata[i][j] = ' ';
				num_pontos--;

				pontuacaoP2 = pontuacaoP2 + P_DONUT;
				glutDisplayFunc(displayNavigateSubwindowP1);
				glutDisplayFunc(displayNavigateSubwindowP2);


				return(GL_TRUE);
			}
		}
		return GL_FALSE;
	}

}
/////////////////////////////////////
//mainWindow

void redisplayAll(void)
{
	glutSetWindow(estado.mainWindow);
	glutPostRedisplay();
	glutSetWindow(estado.topSubwindowP1);
	glutPostRedisplay();
	glutSetWindow(estado.navigateSubwindowP1);
	glutPostRedisplay();
	glutSetWindow(estado.topSubwindowP2);
	glutPostRedisplay();
	glutSetWindow(estado.navigateSubwindowP2);
	glutPostRedisplay();
}

void displayMainWindow()
{
	glClearColor(0.8f, 0.8f, 0.8f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glutSwapBuffers();
}

void Timer(int value)
{
	if (lido = true && num_pontos == 0) {
		if (pontuacaoP1 > pontuacaoP2) {

			printf("O jogador 1 ganhou.\n A fechar em 2 minutos");
			Sleep(120 * 1000);
			exit(1);


		}
		else
		{
			if (pontuacaoP1 < pontuacaoP2) {

				printf("O jogador 2 ganhou.\n A fechar 2 minutos");
				Sleep(120 * 1000);
				exit(1);

			}
			else {
				printf("O jogo ficou empatado.\n A fechar em 2 minutos");
				Sleep(120 * 1000);
				exit(1);

			}

		}

	}

	ALint state;
	alGetSourcei(estado.source[0], AL_SOURCE_STATE, &state);
	if (state == AL_STOPPED)
	{
		alSourcef(estado.source[0], AL_GAIN, 0.3f);
		alSourcei(estado.source[0], AL_BUFFER, estado.buffer[1]);
		alSourcePlay(estado.source[0]);
	}

	GLfloat nx = 0, nz = 0;
	GLfloat nx2 = 0, nz2 = 0;
	GLboolean andar = GL_FALSE;
	GLboolean andar2 = GL_FALSE;

	GLuint curr = glutGet(GLUT_ELAPSED_TIME);
	GLuint curr2 = glutGet(GLUT_ELAPSED_TIME);
	// calcula velocidade baseado no tempo passado
	float velocidade = modelo.objecto.vel*(curr - modelo.prev)*0.001;
	float velocidade2 = modelo.objecto2.vel*(curr - modelo.prev)*0.001;


	modelo.rotacao += 5;


	///////////////////////////////////////////
   // Accoes de reaccao ao teclado Jogador1 //	
	if (modelo.playerP1[JANELA_NAVIGATE].GetSequence() != 20 && modelo.playerP1[JANELA_NAVIGATE2].GetSequence() != 20)
	{
		//glutTimerFunc(estado.timer, Timer, 0);
	}
	else {
		if (value < 4500)
		{
			glutTimerFunc(estado.timer, Timer, value + curr - modelo.prev);
			redisplayAll();
			return;
		}
		else
		{
			modelo.playerP1[JANELA_NAVIGATE].SetSequence(0);
			modelo.playerP1[JANELA_TOP].SetSequence(0);

			modelo.playerP1[JANELA_NAVIGATE2].SetSequence(0);
			modelo.playerP1[JANELA_TOP2].SetSequence(0);
			glutTimerFunc(estado.timer, Timer, 0);
		}
		modelo.prev = curr;
	}

	// Teclas assignadas ao Jogador1
	if (estado.teclas.w || estado.teclas.W) {
		nx = modelo.objecto.pos.x + cos(modelo.objecto.dir)*velocidade;
		nz = modelo.objecto.pos.z + sin(-modelo.objecto.dir)*velocidade;
		/*nx2 = modelo.objecto2.pos.x2 + cos(modelo.objecto2.dir)*OBJECTO_RAIO;
		nz2 = modelo.objecto2.pos.z2 + sin(-modelo.objecto2.dir)*OBJECTO_RAIO;*/
		if (!detectaColisaoP1(nx + cos(modelo.objecto.dir)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir)*OBJECTO_RAIO) &&
			!detectaColisaoP1(nx + cos(modelo.objecto.dir + M_PI / 4)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir + M_PI / 4)*OBJECTO_RAIO) &&
			!detectaColisaoP1(nx + cos(modelo.objecto.dir - M_PI / 4)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir - M_PI / 4)*OBJECTO_RAIO) &&
			!detectaPontoP1(nx + cos(modelo.objecto.dir)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir)*OBJECTO_RAIO) &&
			!detectaPontoP1(nx + cos(modelo.objecto.dir + M_PI / 4)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir + M_PI / 4)*OBJECTO_RAIO) &&
			!detectaPontoP1(nx + cos(modelo.objecto.dir - M_PI / 4)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir - M_PI / 4)*OBJECTO_RAIO)
			) {
			modelo.objecto.pos.x = nx;
			modelo.objecto.pos.z = nz;
		}
		andar = GL_TRUE;
	}
	if (estado.teclas.s || estado.teclas.S) {
		nx = modelo.objecto.pos.x - cos(modelo.objecto.dir)*velocidade;
		nz = modelo.objecto.pos.z - sin(-modelo.objecto.dir)*velocidade;
		if (!detectaColisaoP1(nx + cos(modelo.objecto.dir)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir)*OBJECTO_RAIO) &&
			!detectaColisaoP1(nx + cos(modelo.objecto.dir + M_PI / 4)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir + M_PI / 4)*OBJECTO_RAIO) &&
			!detectaColisaoP1(nx + cos(modelo.objecto.dir - M_PI / 4)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir - M_PI / 4)*OBJECTO_RAIO) &&
			!detectaPontoP1(nx + cos(modelo.objecto.dir)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir)*OBJECTO_RAIO) &&
			!detectaPontoP1(nx + cos(modelo.objecto.dir + M_PI / 4)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir + M_PI / 4)*OBJECTO_RAIO) &&
			!detectaPontoP1(nx + cos(modelo.objecto.dir - M_PI / 4)*OBJECTO_RAIO, nz + sin(-modelo.objecto.dir - M_PI / 4)*OBJECTO_RAIO)
			) {
			modelo.objecto.pos.x = nx;
			modelo.objecto.pos.z = nz;
			andar = GL_TRUE;
		}
	}
	if (estado.teclas.a || estado.teclas.A) {
		modelo.objecto.dir += RAD(OBJECTO_ROTACAO);
		estado.camera.dir_long += RAD(OBJECTO_ROTACAO);
	}
	if (estado.teclas.d || estado.teclas.D) {
		modelo.objecto.dir -= RAD(OBJECTO_ROTACAO);
		estado.camera.dir_long -= RAD(OBJECTO_ROTACAO);
	}
	if (estado.teclas.e || estado.teclas.E) {
		modelo.playerP1[JANELA_NAVIGATE].SetSequence(26);
		modelo.playerP1[JANELA_NAVIGATE2].SetSequence(26);
	}

	if (modelo.playerP1[JANELA_NAVIGATE].GetSequence() != 20 && modelo.playerP1[JANELA_NAVIGATE2].GetSequence() != 20)
	{
		if (andar && modelo.playerP1[JANELA_NAVIGATE].GetSequence() != 3 && modelo.playerP1[JANELA_NAVIGATE2].GetSequence() != 3)
		{
			modelo.playerP1[JANELA_NAVIGATE].SetSequence(3);
			modelo.playerP1[JANELA_TOP].SetSequence(3);

			modelo.playerP1[JANELA_NAVIGATE2].SetSequence(3);
			modelo.playerP1[JANELA_TOP2].SetSequence(3);
		}
		else {
			if (!andar && modelo.playerP1[JANELA_NAVIGATE].GetSequence() != 0 && modelo.playerP1[JANELA_NAVIGATE2].GetSequence() != 0)
			{
				modelo.playerP1[JANELA_NAVIGATE].SetSequence(0);
				modelo.playerP1[JANELA_TOP].SetSequence(0);

				modelo.playerP1[JANELA_NAVIGATE2].SetSequence(0);
				modelo.playerP1[JANELA_TOP2].SetSequence(0);
			}
		}
	}

	///////////////////////////////////////////
   // Accoes de reaccao ao teclado Jogador2 //
	if (modelo.playerP2[JANELA_NAVIGATE].GetSequence() != 20 && modelo.playerP2[JANELA_NAVIGATE2].GetSequence() != 20)
	{
		glutTimerFunc(estado.timer, Timer, 0);
	}
	else
		if (value < 4500)
		{
			glutTimerFunc(estado.timer, Timer, value + curr - modelo.prev);
			redisplayAll();
			return;
		}
		else
		{
			modelo.playerP2[JANELA_NAVIGATE].SetSequence(0);
			modelo.playerP2[JANELA_TOP].SetSequence(0);

			modelo.playerP2[JANELA_NAVIGATE2].SetSequence(0);
			modelo.playerP2[JANELA_TOP2].SetSequence(0);
			glutTimerFunc(estado.timer, Timer, 0);
		}
	modelo.prev = curr;

	// Teclas assignadas ao Jogador2
	if (estado.teclas.i || estado.teclas.I) {
		nx2 = modelo.objecto2.pos.x2 + cos(modelo.objecto2.dir)*velocidade2;
		nz2 = modelo.objecto2.pos.z2 + sin(-modelo.objecto2.dir)*velocidade2;
		if (!detectaColisaoP2(nx2 + cos(modelo.objecto2.dir)*OBJECTO_RAIO, nz2 + sin(-modelo.objecto2.dir)*OBJECTO_RAIO) &&
			!detectaColisaoP2(nx2 + cos(modelo.objecto2.dir + M_PI / 4)*OBJECTO_RAIO, nz2 + sin(-modelo.objecto2.dir + M_PI / 4)*OBJECTO_RAIO) &&
			!detectaColisaoP2(nx2 + cos(modelo.objecto2.dir - M_PI / 4)*OBJECTO_RAIO, nz2 + sin(-modelo.objecto2.dir - M_PI / 4)*OBJECTO_RAIO) &&
			!detectaPontoP2(nx2 + cos(modelo.objecto2.dir)*OBJECTO_RAIO, nz2 + sin(-modelo.objecto2.dir)*OBJECTO_RAIO) &&
			!detectaPontoP2(nx2 + cos(modelo.objecto2.dir + M_PI / 4)*OBJECTO_RAIO, nz2 + sin(-modelo.objecto2.dir + M_PI / 4)*OBJECTO_RAIO) &&
			!detectaPontoP2(nx2 + cos(modelo.objecto2.dir - M_PI / 4)*OBJECTO_RAIO, nz2 + sin(-modelo.objecto2.dir - M_PI / 4)*OBJECTO_RAIO)) {
			modelo.objecto2.pos.x2 = nx2;
			modelo.objecto2.pos.z2 = nz2;

		}
		andar2 = GL_TRUE;
	}
	if (estado.teclas.k || estado.teclas.K) {
		nx2 = modelo.objecto2.pos.x2 - cos(modelo.objecto2.dir)*velocidade2;
		nz2 = modelo.objecto2.pos.z2 - sin(-modelo.objecto2.dir)*velocidade2;
		if (!detectaColisaoP2(nx2, nz2) &&
			!detectaColisaoP2(nx2 - cos(modelo.objecto2.dir + M_PI / 4)*OBJECTO_RAIO, nz2 - sin(-modelo.objecto2.dir + M_PI / 4)*OBJECTO_RAIO) &&
			!detectaColisaoP2(nx2 - cos(modelo.objecto2.dir - M_PI / 4)*OBJECTO_RAIO, nz2 - sin(-modelo.objecto2.dir - M_PI / 4)*OBJECTO_RAIO) &&
			!detectaPontoP2(nx2, nz2) &&
			!detectaPontoP2(nx2 - cos(modelo.objecto2.dir + M_PI / 4)*OBJECTO_RAIO, nz2 - sin(-modelo.objecto2.dir + M_PI / 4)*OBJECTO_RAIO) &&
			!detectaPontoP2(nx2 - cos(modelo.objecto2.dir - M_PI / 4)*OBJECTO_RAIO, nz2 - sin(-modelo.objecto2.dir - M_PI / 4)*OBJECTO_RAIO)) {
			modelo.objecto2.pos.x2 = nx2;
			modelo.objecto2.pos.z2 = nz2;
		}
		andar2 = GL_TRUE;
	}
	if (estado.teclas.j || estado.teclas.J) {
		modelo.objecto2.dir += RAD(OBJECTO_ROTACAO);
		estado.camera2.dir_long += RAD(OBJECTO_ROTACAO);
	}
	if (estado.teclas.l || estado.teclas.L) {
		modelo.objecto2.dir -= RAD(OBJECTO_ROTACAO);
		estado.camera2.dir_long -= RAD(OBJECTO_ROTACAO);
	}
	if (estado.teclas.o || estado.teclas.O) {
		modelo.playerP2[JANELA_NAVIGATE].SetSequence(26);
		modelo.playerP2[JANELA_NAVIGATE2].SetSequence(26);
	}

	if (modelo.playerP2[JANELA_NAVIGATE].GetSequence() != 20 && modelo.playerP2[JANELA_NAVIGATE2].GetSequence() != 20)
	{
		if (andar2 && modelo.playerP2[JANELA_NAVIGATE].GetSequence() != 3 && modelo.playerP2[JANELA_NAVIGATE2].GetSequence() != 3)
		{
			modelo.playerP2[JANELA_NAVIGATE].SetSequence(3);
			modelo.playerP2[JANELA_TOP].SetSequence(3);

			modelo.playerP2[JANELA_NAVIGATE2].SetSequence(3);
			modelo.playerP2[JANELA_TOP2].SetSequence(3);
		}
		else
			if (!andar2 && modelo.playerP2[JANELA_NAVIGATE].GetSequence() != 0 && modelo.playerP2[JANELA_NAVIGATE2].GetSequence() != 0)
			{
				modelo.playerP2[JANELA_NAVIGATE].SetSequence(0);
				modelo.playerP2[JANELA_TOP].SetSequence(0);

				modelo.playerP2[JANELA_NAVIGATE2].SetSequence(0);
				modelo.playerP2[JANELA_TOP2].SetSequence(0);
			}
	}
	redisplayAll();
}




void imprime_ajuda(void)
{
	printf("\n\nJogo Apanha-me se puderes\n");
	printf("h,H - Ajuda \n");
	printf("******* Diversos ******* \n");
	printf("g,G - Alterna o calculo luz entre Z e eye (GL_LIGHT_MODEL_LOCAL_VIEWER)\n");
	printf("y,Y - Wireframe \n");
	printf("t,T - Fill \n");
	printf("******* Movimento P1 ******* \n");
	printf("w- Avanca \n");
	printf("s- Recua \n");
	printf("a- Esquerda\n");
	printf("d- Direita\n");
	printf("******* Movimento P2 ******* \n");
	printf("i- Avanca \n");
	printf("k- Recua \n");
	printf("j- Esquerda\n");
	printf("l- Direita\n");
	printf("******* Camara ******* \n");
	printf("F1 - Alterna camara da janela da Esquerda do P1 \n");
	printf("F2 - Alterna camara da janela da Direita do P1 \n");
	printf("F3 - Alterna camara da janela da Esquerda do P2 \n");
	printf("F4 - Alterna camara da janela da Direita do P2 \n");
	printf("F5 - Ativa/ Desativa nevoeiro do P1 \n");
	printf("F6 - Ativa/ Desativa nevoeiro do P2\n");
	printf("PAGE_UP, PAGE_DOWN - Altera abertura da camara \n");
	printf("botao esquerdo + movimento na Janela da Direita altera o olhar \n");
	printf("ESC - Sair\n");
}

void Key(unsigned char key, int x, int y)
{
	switch (key) {
	case 27:
		exit(1);
		break;
	case 'h':
	case 'H':
		imprime_ajuda();
		break;
	case 'g':
	case 'G':
		estado.localViewer = !estado.localViewer;
		break;
	case 'y':
	case 'Y':
		glutSetWindow(estado.navigateSubwindowP1);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_TEXTURE_2D);
		glutSetWindow(estado.topSubwindowP1);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_TEXTURE_2D);
		break;
	case 't':
	case 'T':
		glutSetWindow(estado.navigateSubwindowP1);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_TEXTURE_2D);
		glutSetWindow(estado.topSubwindowP1);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_TEXTURE_2D);
		break;
	case 'w':
	case 'W':
		estado.teclas.w = GL_TRUE;
		break;
	case 's':
	case 'S':
		estado.teclas.s = GL_TRUE;
		break;
	case 'a':
	case 'A':
		estado.teclas.a = GL_TRUE;
		break;
	case 'd':
	case 'D':
		estado.teclas.d = GL_TRUE;
		break;
	case 'e':
	case 'E':
		estado.teclas.e = GL_TRUE;
		break;
	case 'i':
	case 'I':
		estado.teclas.i = GL_TRUE;
		break;
	case 'k':
	case 'K':
		estado.teclas.k = GL_TRUE;
		break;
	case 'j':
	case 'J':
		estado.teclas.j = GL_TRUE;
		break;
	case 'l':
	case 'L':
		estado.teclas.l = GL_TRUE;
		break;
	case 'o':
	case 'O':
		estado.teclas.o = GL_TRUE;
		break;
	}

}

void KeyUp(unsigned char key, int x, int y)
{
	switch (key) {
	case 'w':
	case 'W':
		estado.teclas.w = GL_FALSE;
		break;
	case 's':
	case 'S':
		estado.teclas.s = GL_FALSE;
		break;
	case 'a':
	case 'A':
		estado.teclas.a = GL_FALSE;
		break;
	case 'd':
	case 'D':
		estado.teclas.d = GL_FALSE;
		break;
	case 'e':
	case 'E':
		estado.teclas.e = GL_FALSE;
		break;
	case 'i':
	case 'I':
		estado.teclas.i = GL_FALSE;
		break;
	case 'k':
	case 'K':
		estado.teclas.k = GL_FALSE;
		break;
	case 'j':
	case 'J':
		estado.teclas.j = GL_FALSE;
		break;
	case 'l':
	case 'L':
		estado.teclas.l = GL_FALSE;
		break;
	case 'o':
	case 'O':
		estado.teclas.o = GL_FALSE;
		break;
	}
}

void SpecialKey(int key, int x, int y)
{
	ALint state;
	switch (key) {
	case GLUT_KEY_UP: estado.teclas.up = GL_TRUE;
		break;
	case GLUT_KEY_DOWN: estado.teclas.down = GL_TRUE;
		break;
	case GLUT_KEY_LEFT: estado.teclas.left = GL_TRUE;
		break;
	case GLUT_KEY_RIGHT: estado.teclas.right = GL_TRUE;
		break;
	case GLUT_KEY_F1: estado.vista[JANELA_TOP] = !estado.vista[JANELA_TOP];
		alGetSourcei(estado.source[3], AL_SOURCE_STATE, &state);
		if (state != AL_PLAYING)
		{
			alSourcei(estado.source[3], AL_BUFFER, estado.buffer[6]);
			alSourcePlay(estado.source[3]);
		}
		break;
	case GLUT_KEY_F2: estado.vista[JANELA_NAVIGATE] = !estado.vista[JANELA_NAVIGATE];
		alGetSourcei(estado.source[3], AL_SOURCE_STATE, &state);
		if (state != AL_PLAYING)
		{
			alSourcei(estado.source[3], AL_BUFFER, estado.buffer[6]);
			alSourcePlay(estado.source[3]);
		}
		break;
	case GLUT_KEY_F3: estado.vista[JANELA_TOP2] = !estado.vista[JANELA_TOP2];
		alGetSourcei(estado.source[4], AL_SOURCE_STATE, &state);
		if (state != AL_PLAYING)
		{
			alSourcei(estado.source[4], AL_BUFFER, estado.buffer[7]);
			alSourcePlay(estado.source[4]);
		}
		break;
	case GLUT_KEY_F4: estado.vista[JANELA_NAVIGATE2] = !estado.vista[JANELA_NAVIGATE2];
		alGetSourcei(estado.source[4], AL_SOURCE_STATE, &state);
		if (state != AL_PLAYING)
		{
			alSourcei(estado.source[4], AL_BUFFER, estado.buffer[7]);
			alSourcePlay(estado.source[4]);
		}
		break;
	case GLUT_KEY_F5: estado.nevoeiro[JOGADOR1] = !estado.nevoeiro[JOGADOR1];
		alGetSourcei(estado.source[3], AL_SOURCE_STATE, &state);
		if (state != AL_PLAYING)
		{
			alSourcei(estado.source[3], AL_BUFFER, estado.buffer[6]);
			alSourcePlay(estado.source[3]);
		}
		break;
	case GLUT_KEY_F6: estado.nevoeiro[JOGADOR2] = !estado.nevoeiro[JOGADOR2];
		alGetSourcei(estado.source[4], AL_SOURCE_STATE, &state);
		if (state != AL_PLAYING)
		{
			alSourcei(estado.source[4], AL_BUFFER, estado.buffer[7]);
			alSourcePlay(estado.source[4]);
		}
		break;
	case GLUT_KEY_PAGE_UP:
		if (estado.camera.fov > 20)
		{
			estado.camera.fov--;
			glutSetWindow(estado.navigateSubwindowP1);
			reshapeNavigateSubwindowP1(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
			redisplayAll();
		}
		break;
	case GLUT_KEY_PAGE_DOWN:
		if (estado.camera.fov < 130)
		{
			estado.camera.fov++;
			glutSetWindow(estado.navigateSubwindowP1);
			reshapeNavigateSubwindowP1(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
			redisplayAll();
		}
		break;
	}

}
// Callback para interaccao via teclas especiais (largar na tecla)
void SpecialKeyUp(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP: estado.teclas.up = GL_FALSE;
		break;
	case GLUT_KEY_DOWN: estado.teclas.down = GL_FALSE;
		break;
	case GLUT_KEY_LEFT: estado.teclas.left = GL_FALSE;
		break;
	case GLUT_KEY_RIGHT: estado.teclas.right = GL_FALSE;
		break;
	}
}

////////////////////////////////////
// Inicializa��es

void createDisplayLists(int janelaID)
{
	modelo.labirinto[janelaID] = glGenLists(2);
	glNewList(modelo.labirinto[janelaID], GL_COMPILE);
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
	desenhaLabirinto(modelo.texID[janelaID][ID_TEXTURA_CUBOS]);
	glPopAttrib();
	glEndList();

	modelo.chao[janelaID] = modelo.labirinto[janelaID] + 1;
	glNewList(modelo.chao[janelaID], GL_COMPILE);
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
	desenhaChao(CHAO_DIMENSAO, modelo.texID[janelaID][ID_TEXTURA_CHAO]);
	glPopAttrib();
	glEndList();
}

///////////////////////////////////
/// Texturas

// Load BMP S� para windows (usa biblioteca glaux)
#ifdef _WIN32

AUX_RGBImageRec *LoadBMP(char *Filename)				// Loads A Bitmap Image
{
	FILE *File = NULL;									// File Handle

	if (!Filename)										// Make Sure A Filename Was Given
	{
		return NULL;									// If Not Return NULL
	}

	File = fopen(Filename, "r");							// Check To See If The File Exists

	if (File)											// Does The File Exist?
	{
		fclose(File);									// Close The Handle
		return auxDIBImageLoad(Filename);				// Load The Bitmap And Return A Pointer
	}

	return NULL;										// If Load Failed Return NULL
}
#endif

void createTextures(GLuint texID[])
{
	char *image;
	int w, h, bpp;

#ifdef _WIN32
	AUX_RGBImageRec *TextureImage[3];					// Create Storage Space For The Texture

	memset(TextureImage, 0, sizeof(void *) * 3);           	// Set The Pointer To NULL
#endif

	glGenTextures(NUM_TEXTURAS, texID);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

#ifdef _WIN32
	if (TextureImage[0] = LoadBMP(NOME_TEXTURA_CUBOS))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CUBOS]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
#else
	if (read_JPEG_file(NOME_TEXTURA_CUBOS, &image, &w, &h, &bpp))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CUBOS]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
#endif
	else
	{
		printf("Textura %s not Found\n", NOME_TEXTURA_CUBOS);
		exit(0);
	}

#ifdef _WIN32
	if (TextureImage[2] = LoadBMP(NOME_TEXTURA_CUBOS2))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CUBOS2]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[2]->sizeX, TextureImage[2]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[2]->data);
	}
#else
	if (read_JPEG_file(NOME_TEXTURA_CUBOS2, &image, &w, &h, &bpp))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CUBOS2]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
#endif
	else
	{
		printf("Textura %s not Found\n", NOME_TEXTURA_CUBOS2);
		exit(0);
	}


	if (read_JPEG_file(NOME_TEXTURA_CHAO, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CHAO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_CHAO);
		exit(0);
	}
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void init()
{
	GLfloat amb[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	estado.timer = 100;
	modelo.rotacao = 50;

	estado.camera.eye.x = 0;
	estado.camera.eye.y = OBJECTO_ALTURA * 2;
	estado.camera.eye.z = 0;
	estado.camera.dir_long = 0;
	estado.camera.dir_lat = 0;
	estado.camera.fov = 60;

	estado.camera2.eye.x2 = 0;
	estado.camera2.eye.y2 = OBJECTO_ALTURA * 2;
	estado.camera2.eye.z2 = 0;
	estado.camera2.dir_long = 0;
	estado.camera2.dir_lat = 0;
	estado.camera2.fov = 60;

	estado.localViewer = 1;
	estado.vista[JANELA_TOP] = 0;
	estado.vista[JANELA_NAVIGATE] = 0;
	estado.vista[JANELA_TOP2] = 0;
	estado.vista[JANELA_NAVIGATE2] = 0;

	int i, j;
	for (i = 0; i < MAZE_HEIGHT; i++) {
		for (j = 0; j < MAZE_WIDTH; j++) {
			if (mazedata[i][j] == 'h') {
				// posicao do boneco P1 no mapa
				modelo.objecto.pos.x = i - 9;
				modelo.objecto.pos.y = OBJECTO_ALTURA*.5;
				modelo.objecto.pos.z = j - 9;
				modelo.objecto.dir = 0;
				modelo.objecto.vel = OBJECTO_VELOCIDADE;
			}
			else if (mazedata[i][j] == 'm') {
				// posicao do boneco P2 no mapa
				modelo.objecto2.pos.x2 = i - 9;
				modelo.objecto2.pos.y2 = OBJECTO_ALTURA*.5;
				modelo.objecto2.pos.z2 = j - 9;
				modelo.objecto2.dir = 0;
				modelo.objecto2.vel = OBJECTO_VELOCIDADE;
			}
		}
	}

	modelo.xMouse = modelo.yMouse = -1;
	modelo.andar = GL_FALSE;

	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_NORMALIZE);  // por causa do Scale do modelo

	if (glutGetWindow() == estado.mainWindow)
		glClearColor(0.8f, 0.8f, 0.8f, 0.0f);
	else
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, amb);
}

/////////////////////////////////////
int main(int argc, char **argv)
{
	//	Carregar mapa
	lerMapa("mapa1");

	glutInit(&argc, argv);
	glutInitWindowPosition(10, 10);
	glutInitWindowSize(MAIN_WIN_WIDTH + GAP * 3, MAIN_WIN_HEIGHT + GAP * 2);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	if ((estado.mainWindow = glutCreateWindow("Jogo do Apanha")) == GL_FALSE)
		exit(1);

	imprime_ajuda();

	// Registar callbacks do GLUT da janela principal
	init();
	InitGL();
	glutReshapeFunc(reshapeMainWindow);
	glutDisplayFunc(displayMainWindow);

	glutTimerFunc(estado.timer, Timer, 0);
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	// criar a sub window topSubwindowP1
	estado.topSubwindowP1 = glutCreateSubWindow(estado.mainWindow, GAP, GAP, TOP_SUB_WIN_WIDTH, TOP_SUB_WIN_HEIGHT);
	init();
	InitGL();
	setLight();
	setMaterial();
	createTextures(modelo.texID[JANELA_TOP]);
	createDisplayLists(JANELA_TOP);

	mdlviewer_init(AVATAR1, modelo.playerP1[JANELA_TOP]);
	mdlviewer_init(AVATAR2, modelo.playerP2[JANELA_TOP]);

	glutReshapeFunc(redisplayTopSubwindowP1);
	glutDisplayFunc(displayTopSubwindowP1);

	glutTimerFunc(estado.timer, Timer, 0);
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	// criar a sub window navigateSubwindowP1
	estado.navigateSubwindowP1 = glutCreateSubWindow(estado.mainWindow, NAV_SUB_WIN_WIDTH + GAP, GAP, NAV_SUB_WIN_WIDTH, NAV_SUB_WIN_HEIGHT);
	init();
	InitGL();
	setLight();
	setMaterial();

	createTextures(modelo.texID[JANELA_NAVIGATE]);
	createDisplayLists(JANELA_NAVIGATE);
	mdlviewer_init(AVATAR1, modelo.playerP1[JANELA_NAVIGATE]);
	mdlviewer_init(AVATAR2, modelo.playerP2[JANELA_NAVIGATE]);

	glutReshapeFunc(reshapeNavigateSubwindowP1);
	glutDisplayFunc(displayNavigateSubwindowP1);
	glutMouseFunc(mouseNavigateSubwindowP1);

	glutTimerFunc(estado.timer, Timer, 0);
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	// criar a sub window topSubwindowP2
	estado.topSubwindowP2 = glutCreateSubWindow(estado.mainWindow, GAP, TOP_SUB_WIN_HEIGHT + GAP, TOP_SUB_WIN_WIDTH, TOP_SUB_WIN_HEIGHT);
	init();
	InitGL();
	setLight();
	setMaterial();
	createTextures(modelo.texID[JANELA_TOP2]);
	createDisplayLists(JANELA_TOP2);
	mdlviewer_init(AVATAR2, modelo.playerP2[JANELA_TOP2]);
	mdlviewer_init(AVATAR1, modelo.playerP1[JANELA_TOP2]);

	glutReshapeFunc(redisplayTopSubwindowP2);
	glutDisplayFunc(displayTopSubwindowP2);

	glutTimerFunc(estado.timer, Timer, 0);
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);


	// criar a sub window navigateSubwindowP2
	estado.navigateSubwindowP2 = glutCreateSubWindow(estado.mainWindow, NAV_SUB_WIN_WIDTH + GAP, NAV_SUB_WIN_HEIGHT + GAP, NAV_SUB_WIN_WIDTH, NAV_SUB_WIN_HEIGHT);
	init();
	InitGL();
	setLight();
	setMaterial();
	createTextures(modelo.texID[JANELA_NAVIGATE2]);
	createDisplayLists(JANELA_NAVIGATE2);
	mdlviewer_init(AVATAR1, modelo.playerP1[JANELA_NAVIGATE2]);
	mdlviewer_init(AVATAR2, modelo.playerP2[JANELA_NAVIGATE2]);

	glutReshapeFunc(reshapeNavigateSubwindowP2);
	glutDisplayFunc(displayNavigateSubwindowP2);
	glutMouseFunc(mouseNavigateSubwindowP2);

	glutTimerFunc(estado.timer, Timer, 0);
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	srand((unsigned)time(NULL));
	alutInit(&argc, argv);	//iniciar sons
	estado.buffer[0] = alutCreateBufferFromFile("sons/welcome.wav");
	estado.buffer[1] = alutCreateBufferFromFile("sons/closetheme.wav");
	estado.buffer[2] = alutCreateBufferFromFile("sons/dohaaa.wav");
	estado.buffer[3] = alutCreateBufferFromFile("sons/doh3.wav");
	estado.buffer[4] = alutCreateBufferFromFile("sons/ltfx_003.wav");
	estado.buffer[5] = alutCreateBufferFromFile("sons/ltfx_017.wav");
	estado.buffer[6] = alutCreateBufferFromFile("sons/smb_pause.wav");
	estado.buffer[7] = alutCreateBufferFromFile("sons/smw_pause.wav");
	estado.buffer[8] = alutCreateBufferFromFile("sons/donuts(1).wav");
	estado.buffer[9] = alutCreateBufferFromFile("sons/donuts(2).wav");
	estado.buffer[10] = alutCreateBufferFromFile("sons/donuts(3).wav");
	estado.buffer[11] = alutCreateBufferFromFile("sons/donuts(4).wav");
	estado.buffer[12] = alutCreateBufferFromFile("sons/smb_1-up.wav");
	estado.buffer[13] = alutCreateBufferFromFile("sons/smb_coin.wav");
	estado.buffer[14] = alutCreateBufferFromFile("sons/smb_powerup.wav");
	alGenSources(7, estado.source);
	alSourcei(estado.source[0], AL_BUFFER, estado.buffer[0]);
	alSourcePlay(estado.source[0]);

	glutMainLoop();
	return 0;

}

